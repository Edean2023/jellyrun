﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreDisplay;
    public int score;

    private void Update()
    {
        // score display is equal to the int score
        scoreDisplay.text = score.ToString();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        // if the player isn't hit by an obstacle
        if (other.CompareTag("Obstacle"))
        {
            // increase score by one
            score++;
        }
    }
}
