﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour
{
    public ScoreManager scoreManager;
    public int currentScore;
    public Text hsDisplay;
    public Player player;

    private void Start()
    {
        // gets the player's high score
        hsDisplay.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
    }
    // Update is called once per frame
    void Update()
    {
        // sets current score the tracked score
        currentScore = scoreManager.score;

        // if the current score is greaterthan what is set as the high score
        if (currentScore > PlayerPrefs.GetInt("HighScore", 0))
        {
            // update the saved high score
            PlayerPrefs.SetInt("HighScore", currentScore);
            // display new highscore
            hsDisplay.text = currentScore.ToString();
        }
    }
}
