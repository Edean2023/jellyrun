﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBG : MonoBehaviour
{
    public float speed;

    public float endX;
    public float startX;
    private void Update()
    {
        // move background to the left
        transform.Translate(Vector2.left * speed * Time.deltaTime);

        // move the background back to the start position after it goes off the screen
        if (transform.position.x <= endX)
        {
            Vector2 pos = new Vector2(startX, transform.position.y);
            transform.position = pos;
        }
    }
}
