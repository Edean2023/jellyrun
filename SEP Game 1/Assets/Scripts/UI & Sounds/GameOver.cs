﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
public class GameOver : MonoBehaviour
{
    public GameObject panel;
    public Player player;
    public Text finalScore;
    public HighScore hs;
    public AudioSource bgMusic;

    private void Start()
    {
        // sets the game over screen to off by default
        panel.SetActive(false);
    }

    private void Update()
    {
        if (player.health <= 0)
        {
            // pause background music
            bgMusic.Pause();
            // turn on the game over screen
            panel.SetActive(true);
            // freeze time
            Time.timeScale = 0;
        }

        // if the game over screen is active
        if (panel.activeInHierarchy)
        {
            // if the player presses the R key
            if (Input.GetKeyDown(KeyCode.R))
            {
                // play background music
                bgMusic.Play();
                // turn off game over panel
                panel.SetActive(false);
                // reload the game scene
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                // unfreeze time
                Time.timeScale = 1;
            }
        }
    }
}
