﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject jellyBoi;

    // plays the game
    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    // opens the option menu 
    public void Options()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
        jellyBoi.SetActive(false);
    }

    // sends you back to the main menu
    public void Back()
    {
        mainMenu.SetActive(true);
        optionsMenu.SetActive(false);
        jellyBoi.SetActive(true);
    }

    // quits game
    // needs to quit in build
    public void Quit()
    {
        Debug.Log("Quit");
    }
}
