﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] obstaclePatterns;

    private float timeBtwSpawns;
    public float startTimeBtwnSpawns;
    public float decreaseTime;
    public float minTime = 0.65f;

    private void Update()
    {
        if (timeBtwSpawns <= 0)
        {
            // spawn random obstacle patterns 
            int rand = Random.Range(0, obstaclePatterns.Length);

            Instantiate(obstaclePatterns[rand], transform.position, Quaternion.identity);
            // slowly decrease the time between spawning obstacles 
            timeBtwSpawns = startTimeBtwnSpawns;

            if (startTimeBtwnSpawns > minTime)
            {
                startTimeBtwnSpawns -= decreaseTime;
            }
        }
        else
        {
            timeBtwSpawns -= Time.deltaTime;
        } 
    }
}
