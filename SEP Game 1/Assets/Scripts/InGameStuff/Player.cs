﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
public class Player : MonoBehaviour
{
    private Vector2 targetPos;
    public float Yincrament;
    public float speed;
    public float maxHeight;
    public float minHeight;
    public int health = 3;
     public GameObject effect;
    public Text healthDisplay;
    public AudioSource moveSound;
    public AudioSource hitSound;

    // Update is called once per frame
    void Update()
    {
        healthDisplay.text = health.ToString();

        // moves the character smoothly
        transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

        // moves the player up
        if (Input.GetKeyDown(KeyCode.W) && transform.position.y < maxHeight)
        {
            moveSound.Play();
            Instantiate(effect, transform.position, Quaternion.identity);
            targetPos = new Vector2(transform.position.x, transform.position.y + Yincrament);
        }
        // moves the player down
        else if (Input.GetKeyDown(KeyCode.S) && transform.position.y > minHeight)
        {
            moveSound.Play();
            Instantiate(effect, transform.position, Quaternion.identity);
            targetPos = new Vector2(transform.position.x, transform.position.y - Yincrament);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Obstacle"))
        {
            hitSound.Play();
        }
    }

}
