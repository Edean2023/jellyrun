﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject obstacle;

    private void Start()
    {
        // spawn obstacle 
        Instantiate(obstacle, transform.position, Quaternion.identity);
    }
}
