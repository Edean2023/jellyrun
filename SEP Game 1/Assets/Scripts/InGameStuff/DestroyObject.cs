﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public GameObject destroyThis;
    public float objectTimer = 2f;

    // Update is called once per frame
    void Update()
    {
        // destroys objects after 5 seconds
        objectTimer = objectTimer - Time.deltaTime;

        if (objectTimer <= 0)
        {
            Destroy(gameObject);
        }
    }
}
