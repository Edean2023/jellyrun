﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Obstacle : MonoBehaviour
{
    public int damage = 1;
    public float speed;
    private float objectTimer = 5f;

    public GameObject effects;

    private void Update()
    {
        // destroys objects after 5 seconds
        objectTimer = objectTimer - Time.deltaTime;

        if(objectTimer <= 0)
        {
            Destroy(gameObject);
        }

        // moves the objects to the left
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // checks if it hit the player
        if (other.CompareTag("Player"))
        {
            // play the particle effect
            Instantiate(effects, transform.position, Quaternion.identity);
            // deal damage to the player and destroy the obstacle
            other.GetComponent<Player>().health--;
           // Debug.Log(other.GetComponent<Player>().health -= damage);
            Destroy(gameObject);
        }


    }
}
